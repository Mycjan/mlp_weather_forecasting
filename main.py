import weather_parser
import numpy as np
import pandas as pd

from numpy import loadtxt
from keras.models import Sequential
from keras.layers import Dense

if __name__ == '__main__':
    df = pd.read_csv("data_combined_bfilled_nans.csv")
    weather_map = weather_parser.create_weather_description_map(df)
    weather_parser.perform_data_normalization(df, weather_map)
    head = df.head(100)
    df = weather_parser.aggregate_dataframe(df, 3)
    print("done")

    #model = Sequential()
    #model.add(Dense(12, input_dim=8, activation='relu'))
    #model.add(Dense(8, activation='relu'))
    #model.add(Dense(1, activation='sigmoid'))
    # compile the keras model
    #model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])
    # fit the keras model on the dataset
    #model.fit(X, y, epochs=150, batch_size=10)
    # evaluate the keras model
    #_, accuracy = model.evaluate(X, y)
    #print('Accuracy: %.2f' % (accuracy*100))

    # test data
    # make probability predictions with the model
    #predictions = model.predict(X)