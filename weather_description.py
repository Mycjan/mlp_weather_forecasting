
def convert_weather_desc_name(weather_desc):
    row = dict()
    row["Rain"] = 0
    row["Thunderstorm"] = 0
    row["Snow"] = 0
    row["Air"] = 0
    row["Clouds"] = 0
    row["Mist"] = 0 
    row["Wind"] = 0

    if weather_desc == "sky is clear":
        row['Rain'] = 1/17

    if weather_desc == "shower drizzle":
        row['Rain'] = 2/17
    if weather_desc == "drizzle":
        row['Rain'] = 3/17
    if weather_desc == "light intensity drizzle":
        row['Rain'] = 4/17
    if weather_desc == "heavy intensity drizzle":
        row['Rain'] = 5/17
    if weather_desc == "light intensity drizzle rain":
        row['Rain'] = 6/17
    if weather_desc == "proximity shower rain":
        row['Rain'] = 7/17
    if weather_desc == "ragged shower rain":
        row['Rain'] = 8/17
    if weather_desc == "shower rain":
        row['Rain'] = 9/17
    if weather_desc == "light intensity shower rain":
        row['Rain'] = 10/17
    if weather_desc == "heavy intensity shower rain":
        row['Rain'] = 11/17
    if weather_desc == "light rain":
        row['Rain'] = 12/17
    if weather_desc == "proximity moderate rain":
        row['Rain'] = 13/17
    if weather_desc == "moderate rain":
        row['Rain'] = 14/17
    if weather_desc == "heavy intensity rain":
        row['Rain'] = 15/17
    if weather_desc == "very heavy rain":
        row['Rain'] = 16/17
    if weather_desc == "freezing rain":
        row['Rain'] = 17/17
    if weather_desc == "light shower sleet": #ragged shower rain + light shower snow
        row['Rain'] = 8/17
        row['Snow'] = 1/6
        #Thunderstorm
    if weather_desc == "proximity thunderstorm":
        row["Thunderstorm"] = 1/2
    if weather_desc == "thunderstorm":
        row["Thunderstorm"] = 2/2
    if weather_desc == "thunderstorm with rain":
        row["Thunderstorm"] = 2/2
        row['Rain'] = 14 / 17
    if weather_desc == "thunderstorm with heavy rain":
        row["Thunderstorm"] = 2 / 2
        row['Rain'] = 15 / 17
    if weather_desc == "thunderstorm with light rain":
        row["Thunderstorm"] = 2 / 2
        row['Rain'] = 12 / 17
    if weather_desc == "proximity thunderstorm with rain":
        row["Thunderstorm"] = 1 / 2
        row['Rain'] = 14 / 17
    if weather_desc == "proximity thunderstorm with drizzle":
        row["Thunderstorm"] = 1 / 2
        row['Rain'] = 3 / 17
    if weather_desc == "thunderstorm with heavy drizzle":
        row["Thunderstorm"] = 2 / 2
        row['Rain'] = 5 / 17
    if weather_desc == "thunderstorm with light drizzle":
        row["Thunderstorm"] = 2 / 2
        row['Rain'] = 4 / 17
    if weather_desc == "thunderstorm with drizzle":
        row["Thunderstorm"] = 2 / 2
        row['Rain'] = 3 / 17
        #Snow
    if weather_desc == "light shower snow":
        row["Snow"] = 1 / 6
    if weather_desc == "shower snow":
        row["Snow"] = 2 / 6
    if weather_desc == "light snow":
        row["Snow"] = 3 / 6
    if weather_desc == "snow":
        row["Snow"] = 4 / 6
    if weather_desc == "heavy shower snow":
        row["Snow"] = 5 / 6
    if weather_desc == "heavy snow":
        row["Snow"] = 6 / 6
    if weather_desc == "light rain and snow":
        row["Snow"] = 3 / 6
        row["Rain"] = 12 / 17
        #Air
    if weather_desc == "dust" or weather_desc == "volcanic ash":
        row["Air"] = 1 / 5
    if weather_desc == "smoke":
        row["Air"] = 2 / 5
    if weather_desc == "sand":
        row["Air"] = 3 / 5
    if weather_desc == "proximisty sand/dust whirles":
        row["Air"] = 4 / 5
    if weather_desc == "sand/dust whirls":
        row["Air"] = 5 / 5
        #Clouds
    if weather_desc == "scattered clouds":
        row["Clouds"] = 1 / 4
    if weather_desc == "broken clouds":
        row["Clouds"] = 2 / 4
    if weather_desc == "few clouds":
        row["Clouds"] = 3 / 4
    if weather_desc == "overcast clouds":
        row["Clouds"] = 4 / 4
        #Mist
    if weather_desc == "mist":
        row["Mist"] = 1 / 3
    if weather_desc == "haze":
        row["Mist"] = 2 / 3
    if weather_desc == "fog":
        row["Mist"] = 3 / 3
        #Wind
    if weather_desc == "squalls":
        row["Wind"] = 1 / 2
    if weather_desc == "tornado":
        row["Wind"] = 2 / 2










