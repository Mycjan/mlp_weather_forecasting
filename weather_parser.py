import pandas as pd
import numpy as np
import weather_description as wd
max_pressure = 1086
min_pressure = 870
max_temperature = 329
min_wind_speed = 0
min_latitude = -90
max_latitude = 90
min_longitude = -180
max_longitude = 180


def parse_file(file_name: str) -> pd.DataFrame:
    return pd.read_csv(file_name, sep=";")


def parse_and_combine_weather_files() -> pd.DataFrame:
    humidity = parse_file("projekt 4 - dane//train//humidity_train.csv")
    humidity.fillna(method="bfill", inplace=True)
    pressure = parse_file("projekt 4 - dane//train//pressure_train.csv")
    pressure.fillna(method="bfill", inplace=True)
    temperature = parse_file("projekt 4 - dane//train//temperature_train.csv")
    temperature.fillna(method="bfill", inplace=True)
    weather_description = parse_file("projekt 4 - dane//train//weather_description_train.csv")
    weather_description.fillna(method="bfill", inplace=True)
    wind_direction = parse_file("projekt 4 - dane//train//wind_direction_train.csv")
    wind_direction.fillna(method="bfill", inplace=True)
    wind_speed = parse_file("projekt 4 - dane//train//wind_speed_train.csv")
    wind_speed.fillna(method="bfill", inplace=True)
    city_attributes = parse_file("projekt 4 - dane//city_attributes.csv")

    columns = ["DateTime", "CityName", "Humidity", "Pressure", "Temperature", "Rain", "Thunderstorm",
               "Snow", "Air", "Clouds", "Mist", "Wind" "WindDirection", "WindSpeed", "CityLongitude", "CityLatitude"]

    row_count = len(humidity.index)
    city_names = city_attributes["City"].to_list()
    data_table = list()
    for idx in range(row_count):
        dateTime = humidity["datetime"][idx]
        if idx % 100 == 0:
            print(idx)
        for city in city_names:
            humidityV = humidity[city][idx]
            pressureV = pressure[city][idx]
            temperatureV = temperature[city][idx]
            weather_descriptionV = wd.convert_weather_desc_name(weather_description[city][idx])
            wind_directionV = wind_direction[city][idx]
            wind_speedV = wind_speed[city][idx]
            latitude = city_attributes.loc[city_attributes["City"] == city, "Latitude"].iloc[0]
            longitude = city_attributes.loc[city_attributes["City"] == city, "Longitude"].iloc[0]
            data_table.append([dateTime, city, humidityV, pressureV, temperatureV, weather_descriptionV["Rain"],
                               weather_descriptionV["Thunderstorm"], weather_descriptionV["Snow"],
                               weather_descriptionV["Air"], weather_descriptionV["Clouds"],
                               weather_descriptionV["Mist"], weather_descriptionV["Wind"], wind_directionV, wind_speedV,
                               longitude, latitude])

    df = pd.DataFrame(data_table, columns=columns)
    return df


def save_dataframe_to_csv(df: pd.DataFrame, filename: str):
    df.to_csv(filename, index=False)


def remove_empties_from_df(df: pd.DataFrame):
    df = df.replace('', np.nan)
    df.dropna(how='any', inplace=True)


def perform_data_mapping_and_normalization(df: pd.DataFrame):
    first_humidity_value = 33
    second_humidity_value = 66
    df["Humidity"] = df["Humidity"].apply(lambda x: 0 if x < first_humidity_value else
                                          (0.5 if x < second_humidity_value else 1))

    first_wind_dir_value = 120
    second_wind_dir_value = 240
    df["WindDirection"] = df["WindDirection"].apply(lambda x: 0 if x < first_wind_dir_value else
                                                    (0.5 if x < second_wind_dir_value else 1))
    pressure_diff = max_pressure - min_pressure
    first_pressure_value = min_pressure + pressure_diff
    second_pressure_value = first_pressure_value + pressure_diff
    df["Pressure"] = df["Pressure"].apply(lambda x: 0 if x < first_pressure_value else
                                          (0.5 if x < second_pressure_value else 1))
    min_temperature = df["Temperature"].min(skipna=True) - 20
    df["Temperature"] = df["Temperature"].apply(lambda x: (x - min_temperature) / (max_temperature - min_temperature))
    df["WindSpeed"] = df["WindSpeed"].apply(lambda x: 1 if x >= 8 else 0)
    df["DateTime"] = pd.to_datetime(df['Datetime'])
    df.sort_values(["CityName", "DateTime"], ascending=[True, True], inplace=True)
    df.reset_index(drop=True, inplace=True)


def perform_data_normalization(df: pd.DataFrame):
    df["Humidity"] = df["Humidity"].apply(lambda x: x / 100)
    df["WindDirection"] = df["WindDirection"].apply(lambda x: x / 360)
    df["Pressure"] = df["Pressure"].apply(lambda x: (x - min_pressure) / (max_pressure - min_pressure))
    min_temperature = df["Temperature"].min(skipna=True) - 20
    df["Temperature"] = df["Temperature"].apply(lambda x: (x - min_temperature) / (max_temperature - min_temperature))
    max_wind_speed = df["WindSpeed"].max(skipna=True) + 5
    df["WindSpeed"] = df["WindSpeed"].apply(lambda x: (x - min_wind_speed) / (max_wind_speed - min_wind_speed))
    df["CityLatitude"] = df["CityLatitude"].apply(lambda x: (x - min_latitude) / (max_latitude - min_latitude))
    df["CityLongitude"] = df["CityLongitude"].apply(lambda x: (x - min_longitude) / (max_longitude - min_longitude))
    df["DateTime"] = pd.to_datetime(df['DateTime'])
    df.sort_values(["CityName", "DateTime"], ascending=[True, True], inplace=True)
    df.reset_index(drop=True, inplace=True)


def aggregate_dataframe(df: pd.DataFrame, n: int) -> pd.DataFrame:
    d = {"DateTime": 'last', "CityName": 'last', "Humidity": 'mean', "Pressure": 'mean', "Temperature": 'mean',
         "Rain": 'mean', "Thunderstorm": 'mean', "Snow": 'mean', "Air": 'mean', "Clouds": 'mean', "Mist": 'mean',
         "Wind": 'mean', "WindDirection": 'mean', "WindSpeed": 'mean', "CityLongitude": 'mean',
         "CityLatitude": 'mean'}
    return df.groupby(df.index // n).agg(d)




